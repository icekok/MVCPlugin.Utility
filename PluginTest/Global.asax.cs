﻿using MVCPlugin.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PluginTest
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static ILogger Logger = new ConsoleLogger();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            try
            {
                Type type = Type.GetType("MVCPlugin.Utility.Application, MVCPlugin.Utility", true);
                var pro = type.GetField("Instance", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null);
                type.GetMethod("DoStart", BindingFlags.NonPublic | BindingFlags.Instance).Invoke(pro, new object[] { this });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            
        }
    }
}