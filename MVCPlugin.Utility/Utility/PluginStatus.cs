﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVCPlugin.Utility
{
    public enum PluginStatus
    {
        JustDeployed,
        ToInstall,
        Installed,
        ToUninstall
    }
}
