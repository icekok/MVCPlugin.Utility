﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Web.Hosting;

namespace MVCPlugin.Utility
{
    public interface IPlugin
    {
        void Initialize(Application application, PluginMetadata metadata);

        void Install(PluginMetadata metadata);

        void Uninstall(PluginMetadata metadata);
    }    
}