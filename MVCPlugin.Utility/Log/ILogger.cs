﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Web;
using System.Net;
using System.Threading;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace MVCPlugin.Utility
{
    public interface ILogger
    {
        void Error(Exception ex );

        void Error(string errorMsg );

        void Info(string infoMsg );

        void Warning(string warningMsg );
    }
}