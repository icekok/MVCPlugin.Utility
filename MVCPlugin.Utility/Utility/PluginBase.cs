﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;
using System.Web.Mvc;

namespace MVCPlugin.Utility
{
    public abstract class PluginBase : IPlugin
    {
        public virtual void Install(PluginMetadata metadata)
        {

        }

        public virtual void Uninstall(PluginMetadata metadata)
        {

        }

        public void Initialize(Application application, PluginMetadata metadata)
        {
            application.Starting += (s, e) =>
            {
                OnApplicationStarting(metadata);
            };

            application.Started += (s, e) =>
            {
                OnApplicationStarted(metadata);
            };

            application.Stopping += (s, e) =>
            {
                OnApplicationStopping(metadata);
            };

            application.Stopped += (s, e) =>
            {
                OnApplicationStopped(metadata);
            };

            application.Error += (s, e) =>
            {
                OnApplicationError(metadata, e.Exception);
            };

            application.BeginRequest += (s, e) =>
            {
                OnBeginRequest(metadata);
            };

            application.EndRequest += (s, e) =>
            {
                OnEndRequest(metadata);
            };
        }

        protected virtual void OnBeginRequest(PluginMetadata metadata)
        {

        }

        protected virtual void OnEndRequest(PluginMetadata metadata)
        {

        }

        protected virtual void OnApplicationStarting(PluginMetadata metadata)
        {

        }

        protected abstract void OnApplicationStarted(PluginMetadata metadata);

        protected virtual void OnApplicationStopping(PluginMetadata metadata)
        {

        }

        protected virtual void OnApplicationStopped(PluginMetadata metadata)
        {

        }

        protected virtual void OnApplicationError(PluginMetadata metadata, Exception ex)
        {

        }
    }
}
